# CMO OsPoT Framework

This repository contains OsPoT2, the final version of the
enhanced original OsPoT Framework. It comprises of the
necessary XSLT files to transform the TEI-styled XML output
of the CTE (Classical Text Editor) to convert the critical
text edition of the manuscript with the CMO ID NE204.

The main objective of the transformation is to
* split the single file export into individual TEI files,
* clean the export from unneccesary formatting information, and
* enrich the resulting TEI with semantically correct information. 

The order of the transformation is given by the file names:
* 01cleanUp
* 02split
* 03getSigla
* 04buildApparati
* 05divideOrigFromTrans
* 06mergeAdjacentHi
* 07lineMaker
* 08refineMetadata
* 09italic

The ANT Script "buildEverything" runs the entire 
transformation, and "CleanAndSplit" runs the XSLT files
"01cleanUp" and "02split", and "BeautifulOutput" the rest
of them.

The CTE output gives one single file that contains all the
edited lyrics and their respective critical apparatus.
After the transformation, there are seperate XML files for
every single block text lyrics related to a musical piece
that contains specific header information for the individual
lyrics.

Each file of the transformation fulfills a specific purpose
and comprises of the following steps:

## 01cleanUp
* Remove rendition information that are simple CSS, e.g.:
    * font-family,
    * font-size,
    * color,
    * line-hights;
* remove language attributes because they do not provide
information about the language used, but about the
language of the keyboard used,
* clean empty rend elements (without text and children), and
* make every child of "body" to "p".

## 02split
* Split the cleaned document where the piece number
information is located,
* extract piece number and page number as filename,
* build header with those basic information and structure
that can be coded hard:
    * editors,
    * projectDesc, and
    * timeStamp,
    * metDecl;
* build the sections
    * header,
    * body, and 
    * back for further transformation.

## 03getSigla
* Create the list of witnesses in header,
* generate the "wit" for the list out of the annotations,
in the edited lyric and put them into the header,
* get information about witnesses from source catalogue, and
* change CTE ID for witnesses to source catalogue ID

## 04buildApparati
* Create "idno" for every lyrical piece,
* for witnesses from source catalogue, change IDs within,
anchors and apparati to IDs from catalogue,
* create provenance apparatus,
* create variant readings apparatus,
* create annotation apparatus,
* variant readings are "rdg" elements,
* supress notes within the text, and
* remove of @n in first "p" of body.

## 05divideOrigFromTrans
* Get half count of "p" elements in body, if total count is
even, otherwise false (deprecated, as Arabic script is now
ommitted),
* get first "div" around the first half of "p" for the
original text (deprecated),
* get second "div" around the second half of "p" for the
transcribed text,
* special case for pieces continued over one page in the CTE
is catched.

## 06mergeAdjacentHi
* Merge adjacent "hi" elements with identical @rend values.

## 07linemaker
* Transform "p" to "l",
* transform first "p" in a "div" to "head",
* fill piece title in header,
* get text out of "hi" without attributes and give it to the
parent, and
* remove "hi" in original Arabic that contain only the RTL
rendition (deprecated). 

## 08refineMetadata
* List item that matches the piece number,
* get author from transcribed title,
* get transcribed title without author, and
* read makâm from list and put it into "note" in "titleStmt".

## 09italic
* Transformation of the visual formatting of the printed
edition into semantic information:
    * all text is put into "seg" elements,
    * text in bold gets @ana "mainPartLyrics",
    * text in regular font gets @ana "terennüm",
    * text in italic is additionally rendered as "stage" element, and
    * square brackets are rendered as "supplied" element;
* final clean up
    